from flask import render_template
from app import app

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Mark'}
    posts = [
        {
            'author': {'username': 'JC'},
            'body': 'Sunny day in Makati!'
        },
        {
            'author': {'username': 'Erik'},
            'body': 'The Last Jedi sucked!'
        }
    ]
    return render_template('index.html', title='Adobo Connection', user=user, posts=posts)